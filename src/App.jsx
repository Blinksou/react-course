import SeasonCard from "./components/Season/SeasonCard";
import Card from "./components/Card";
import Modal from "./components/Modal";
import { lazy, useState, Suspense } from "react";
import { createUseStyles, ThemeProvider } from "react-jss";
import ThemeButton from "./components/ThemeButton/ThemeButton";
import { getCurrentSeason } from "../lib/season-helper";

const NextSeasonCard = lazy(() => import("./components/NextSeasonCard"));

function App() {
  const [season] = useState(getCurrentSeason());

  const [isModalOpen, setIsModalOpen] = useState(false);
  const [theme, setTheme] = useState("dark");

  const useStyles = createUseStyles({
    root: {
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      fontFamily: "system-ui",
      height: "100vh",
      backgroundColor: ({ theme }) => (theme === "light" ? "white" : "black"),
      color: ({ theme }) => (theme === "light" ? "black" : "white"),
    },
    header: {
      width: "100%",
      display: "flex",
      justifyContent: "flex-end",
    },
    main: {
      display: "flex",
      flexDirection: "column",
      justifyContent: "center",
      alignItems: "center",
    },
    content: {
      marginTop: 8,
      marginBottom: 8,
    },
    actions: {
      marginTop: 8,
      marginBottom: 8,
    },
  });

  const toggle = () => setTheme(theme === "dark" ? "light" : "dark");
  const classes = useStyles({ theme });

  return (
    <ThemeProvider theme={{ type: theme, toggle }}>
      <div className={classes.root}>
        <header className={classes.header}>
          <ThemeButton />
        </header>

        <main className={classes.main}>
          <Card>
            <SeasonCard season={season} onClick={() => setIsModalOpen(true)} />
          </Card>
          <Modal isOpen={isModalOpen} onClose={() => setIsModalOpen(false)}>
            <Card>
              <Suspense fallback={<div>Loading</div>}>
                <NextSeasonCard name={season.next} />
              </Suspense>
            </Card>
          </Modal>
        </main>
      </div>
    </ThemeProvider>
  );
}

export default App;
