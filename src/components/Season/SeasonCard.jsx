import React from "react";
import { useState } from "react";
import { getSeasonData } from "../../../lib/season-helper";

function SeasonCard({ season, onClick }) {
  const startDate = new Date(season.startDate);
  const now = new Date();

  const diffDays = Math.ceil(Math.abs(now - startDate) / (1000 * 60 * 60 * 24));

  return (
    <>
      <h2>{season.name}</h2>
      <p>{`From ${diffDays} days`}</p>
      <button onClick={onClick}>And next ?</button>
    </>
  );
}

export default SeasonCard;
