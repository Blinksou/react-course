import { render, screen } from "@testing-library/react";

import React from "react";
import SeasonCard from "./SeasonCard";
require("@testing-library/jest-dom");

describe(SeasonCard.name, () => {
  describe("", () => {
    it("renders a title `Winter`", () => {
      render(<SeasonCard name={"winter"} />);
      expect(screen.queryByText("Winter")).toBeInTheDocument();
    });
    it("renders a text `From 52 days`", () => {
      render(<SeasonCard name={"winter"} />);
      expect(screen.queryByText("From 52 days")).toBeInTheDocument();
    });
  });
});
