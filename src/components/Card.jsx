function Card({ children }) {
  return (
    <div
      style={{
        border: "2px solid black",
        padding: 20,
        width: "300px",
        textAlign: "center",
      }}
    >
      {children}
    </div>
  );
}

export default Card;
