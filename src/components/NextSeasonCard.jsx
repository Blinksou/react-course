import { formatDistanceStrict, formatDistanceToNowStrict } from "date-fns";
import { useState } from "react";
import { getSeasonData } from "../../lib/season-helper";

function NextSeasonCard({ name }) {
  const [nextSeason] = useState(getSeasonData(name));

  const startDate = new Date(nextSeason.startDate);
  const endDate = new Date(nextSeason.endDate);

  return (
    <>
      <h2>{nextSeason.name}</h2>
      <p style={{ color: "red", fontWeight: "bold" }}>
        {formatDistanceStrict(startDate, endDate, { unit: "day" })}
      </p>
      <p> In {formatDistanceToNowStrict(startDate, { unit: "day" })} </p>
    </>
  );
}

export default NextSeasonCard;
