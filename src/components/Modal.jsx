import { createPortal } from "react-dom";

const OVERLAY_STYLES = {
  position: "fixed",
  top: 0,
  left: 0,
  right: 0,
  bottom: 0,
  backgroundColor: "rgba(0, 0, 0, .7)",
  zIndex: 9999,
};

const MODAL_STYLES = {
  position: "fixed",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  backgroundColor: "#FFF",
  padding: 50,
  zIndex: 9999,
};

function Modal({ isOpen, children, onClose }) {
  if (!isOpen) return null;
  return createPortal(
    <>
      <div style={OVERLAY_STYLES} />
      <div style={MODAL_STYLES}>
        <button onClick={onClose}>Close modal</button>
        {children}
      </div>
    </>,
    document.getElementById("portal")
  );
}

export default Modal;
