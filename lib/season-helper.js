import { data as seasons } from "../src/seasons.json";

export function getCurrentSeason() {
  const now = new Date().getTime();

  return Object.entries(seasons).filter(([name, season]) => {
    const startDate = new Date(season.startDate).getTime();
    const endDate = new Date(season.endDate).getTime();

    if (endDate > now && startDate < now) {
      console.log(name);
      return name;
    }

    return null;
  })[0][1];
}

export function getSeasonData(name) {
  return seasons[name];
}
